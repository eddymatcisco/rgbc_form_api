var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient
var db;
//Establish Connection
MongoClient.connect('mongodb://localhost:27017/mydb', function (err, database) {
   if (err){ 
   	throw err
   }else{
    db = database;
    console.log('Connected to MongoDB');
	 }
 });

var VerifyToken = require('./VerifyToken');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

/**
 * Configure JWT
 */
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var bcrypt = require('bcryptjs');
var config = require('../config'); // get config file

router.post('/register', function(req, res) {
  var hashedPassword = bcrypt.hashSync(req.body.password, 8);
  console.log('Hashed', hashedPassword)

  // if user is registered without errors
    // create a token
    var token = jwt.sign({ id: hashedPassword}, config.secret, {
      expiresIn: 86400 // expires in 24 hours fix => Todo  10 minutes
    });

    res.status(200).send({ auth: true, token: token });
});

router.post('/form', VerifyToken, function(req, res, next) {
  db.collection('store_data').insert(req.body, function (err, result) {
    if (err)
       res.status(500).send('Error');
    else
       console.log(req.body)
       res.status(200).send('Success');
  });
});



module.exports = router;