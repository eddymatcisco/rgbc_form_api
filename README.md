Server API

The server API is a very simple App

to run it :

1. Need to install all the dependancy by running `npm install`
   in case of a missing dependancy run `npm i <dependancy-name> --save`

2. To run the application type `npm start`
   The application should be starting on port 8081


on the app.js you can white list you website on the cors configuration


The is tow endpoint:

1. /api/auth/register

   This should be called by the app each time the user want to access the form.
   the endpoint will return a token which will be passed as a query parameter along with other parameter

   the is currently to expire in 24h but on production it must be set to 10 min

   example : http://<webportalurl:3001>/userId/userName/regionName/storeName/storeCode/accesstoken

2. /api/auth/from
	
	This endpoint will validate the token and save the data into the database
