var express = require('express');
var app = express();
var cors = require('cors');
global.__root   = __dirname + '/'; 

const corsOptions = {
  origin: 'http://localhost:3001'
}

app.use(cors(corsOptions))

app.get('/api', function (req, res) {
  res.status(200).send('API works.');
});

var AuthController = require(__root + 'auth/AuthController');
app.use('/api/auth', AuthController);

module.exports = app;